
#include <stdio.h>
#include <stdlib.h>

/*

2 1 4 3 4
initiate  1 1 4
request   1 1 1
release   1 1 1
terminate 1 0 0
initiate  2 1 4
request   2 1 1
release   2 1 1 terminate 2 0 0
invalid 0 1

*/

/*
Wraps an integer so that it can be marked as valid ( >0 ) or invalid ( < 0 );
*/
struct int_wrapper {
  int value;
  int valid;
};

/*
Wraps a string so that it can be marked as valid ( >0 ) or invalid ( < 0 );
*/
struct str_wrapper {
  char* value;
  int valid;
};

/*
Data Model for a header. Technically header is variable so should be changed to int[].
*/
struct header {
  int* resources;
  int numberOfResources;
};

/*
Data model for a task. **You really should rename my stuff so its not i1,i2,i3
*/
struct task {
  char* cmd;
  int i1;
  int i2;
  int i3;
};

/*
Constructor for the int_wrapper.
Lets me not have to type these 4 lines all over the place.
*/
struct int_wrapper* Les_MakeIWrapper(int value, int valid){
  struct int_wrapper* iw = malloc(sizeof(struct int_wrapper)*1);
  iw->value = value;
  iw->valid = valid;
  return iw;
}

/*
Constructor for the str_wrapper.
Lets me not have to type these 4 lines all over the place.
*/
struct str_wrapper* Les_MakeSWrapper(char* value, int valid){
  struct str_wrapper* sw = malloc(sizeof(struct str_wrapper)*1);
  sw->value = value;
  sw->valid = valid;
  return sw;
}

/*
Gets the next printable character. If you look at an ASCII table, space is the last unprintable character.
Returns -1 if the file is EOF.
*/
int nextPrintable(FILE* fp){
  int nextChar = -1;
  while((nextChar = fgetc(fp)) > 0){
    if(nextChar > ' '){
      break;
    }
  }
  return nextChar;
}

/*
Decodes an Int from the next printable characters in the file. If the next printable characters aren't numeric, it returns an invalid int_wrapper.
*/
struct int_wrapper* readInt(FILE* fp){
  char input[11]; /* 2,147,483,647 is 10 digits long */
  input[11] = '\0';

  int nextChar = nextPrintable(fp);
  if(nextChar < 0 || nextChar < '0' || nextChar > '9'){
    return Les_MakeIWrapper(0,-1);
  }
  input[0] = (char)nextChar;
  int i;
  for(i=1;i<10;i++){
    input[i] = (char)fgetc(fp);
    if( input[i] < '0' || input[i] > '9'){
      input[i] = '\0';
      break;
    }
  }
  return Les_MakeIWrapper(atoi(input),1);
}

/* Reads in a string of, at most, given length or stops when hitting an unprintable character. If the file was EOF, returns an invalid str_wrapper */
struct str_wrapper* readString(FILE* fp, int length){
  char* str = malloc(sizeof(char)*(length + 1));
  str[length] = '\0';
  int nextChar = nextPrintable(fp);
  if(nextChar < 0){
    str[0] = '\0';
    return Les_MakeSWrapper(str,-1);
  }

  str[0] = nextChar;
  int i;
  for(i=1;i<length;i++){
    if((nextChar = fgetc(fp)) < 0){
      str[i] = '\0';
      return Les_MakeSWrapper(str,1);
    }
    if(nextChar <= ' '){
      str[i] = '\0';
      return Les_MakeSWrapper(str,1);
    }
    str[i] = (char)nextChar;
  }
  return Les_MakeSWrapper(str,1);
}

struct header* readHeader(FILE* fp){
  struct int_wrapper* iw;
  int* iws = malloc(sizeof(int)*2);
  int iws_size = 0;
  int iws_max_size = 2;

  while((iw = readInt(fp))->valid > 0){
    if(iws_size == iws_max_size){
      iws_max_size *= 2;
      iws = realloc(iws,sizeof(int)*iws_max_size);
    }
    iws[iws_size] = iw->value;
    iws_size += 1;
  }

  fseek(fp, -1, SEEK_CUR); /* Since readInt was invalid we stole a char from a command */

  if(iws_size == 0){
    /* Invalid Header, probs should stop and return some message */
    return (struct header*)-1;
  }

  printf("Header was: ");
  int i;
  for(i=0;i<iws_size;i++){
    printf("[%d] ",iws[i]);
  }
  printf("\n");

  struct header* header = malloc(sizeof(struct header)*1);
  header->resources = iws;
  header->numberOfResources = iws_size;

  return header;
}

struct task* readCommand(FILE* fp){
  struct str_wrapper* sw1;
  struct int_wrapper* iw1;
  struct int_wrapper* iw2;
  struct int_wrapper* iw3;

  sw1 = readString(fp,25);
  iw1 = readInt(fp);
  iw2 = readInt(fp);
  iw3 = readInt(fp);

  if(sw1->valid < 0 || iw1->valid < 0 || iw2->valid <0 || iw3->valid < 0){
    /* Invalid Header, probs should stop and return some message */
    return (struct task*)-1;
  }

  char* name = sw1->value;
  int i1 = iw1->value;
  int i2 = iw2->value;
  int i3 = iw3->value;

  printf("Cmd was: [%s] [%d] [%d] [%d]\n",name,i1,i2,i3);

  struct task* task = malloc(sizeof(struct task)*1);
  task->cmd = name;
  task->i1 = i1;
  task->i2 = i2;
  task->i3 = i3;

  return task;
}

int main()
{

  FILE *fp;
  fp = fopen("input", "r");

  struct header* h;
  h = readHeader(fp);

  struct task* t;
  while(((int)(t = readCommand(fp))) > 0){ /* Have to cast to int because pointers can't be less than 0 */
  printf("This task was in proc [%d]\n", t->i1);
}

fclose(fp);
return 0;
}
